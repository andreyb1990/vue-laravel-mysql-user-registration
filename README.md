# Base project

Docker running Nginx, Composer, Laravel, Vue.js MySQL.

## Install prerequisites

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important are :

* [Vue CLI](https://cli.vuejs.org)
* [Composer](https://getcomposer.org)
* [Docker](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install/)

Check if `docker-compose` is already installed by entering the following command :

```sh
which docker-compose
```

Check Docker Compose compatibility :

- [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

## Run the application

1. Create the `.env` file in the root and backend directories using the `.env.example` files


2. Install initial composer dependencies

    ```sh
    cd backend && composer install && cd ..
    ```

3. Start the application :

    ```sh
    docker-compose up -d
    ```
   
   **Please wait this might take a several minutes...**


4. Run migrations
   ```sh
    docker exec -it base_backend php artisan migrate
    ```

5. Open [http://localhost](http://localhost/) in the browser


6. Stop and clear services

    ```sh
    sudo docker-compose down -v
    ```