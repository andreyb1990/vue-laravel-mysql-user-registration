import _ from 'lodash';
import { mapGetters } from 'vuex';

export const DataParse = {
    created() {
        this.parseData();
    },
    methods: {
        parseData() {
            const data = this.$store.getters.getData(this.dataObject);
            for (let field in data) {
                if (data[field]) {
                    this.data[field].value = data[field].trim();
                }
            }
        }
    }
};

export const DataSubmit = {
    data: () => {
        return {
            userId: null
        };
    },
    computed: Object.assign(
        mapGetters({
            totalSteps: 'getRegistrationTotalSteps'
        }),
        {
            currentStep() {
                return +this.$route.params.current_step;
            },
            isPenultimateStep() {
                return this.totalSteps - this.currentStep === 1;
            }
        }
    ),
    methods: {
        submit() {
            let data = {};
            Object.keys(this.data).map((key) => {
                return (data[key] = this.data[key].value);
            });
            this.$store.commit(this.event, data);

            this.createUser()
                .then(() => this.createPayment())
                .finally(() => {
                    this.$store.commit('setRegistrationStep', this.currentStep + 1);
                    this.$emit('goNextStep');
                });
        },
        createUser() {
            if (!this.isPenultimateStep) return Promise.resolve();
            const userContacts = this.$store.getters.getData('userContacts');
            const userAddress = this.$store.getters.getData('userAddress');
            const data = {
                first_name: userContacts.firstName,
                last_name: userContacts.lastName,
                phone: userContacts.phone,
                address: {
                    street: userAddress.street,
                    house_number: userAddress.houseNumber,
                    zip_code: userAddress.zipCode,
                    city: userAddress.city
                }
            };
            return this.apiPost('user', data, { json: true })
                .then((response) => {
                    this.userId = response.data.id;
                })
                .catch((error) => console.log(error));
        },
        createPayment() {
            if (!this.userId) return Promise.resolve();

            let data = _.cloneDeep(this.$store.getters.getData('payment'));
            data.user_id = this.userId;

            return this.apiPost('payment', data)
                .then((response) => {
                    if (response?.data?.status === 'success') {
                        this.$store.commit('setPaymentDataId', response.data.payment_data_id);
                    }
                })
                .catch((error) => console.log(error));
        }
    }
};
