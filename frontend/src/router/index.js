import Vue from 'vue';
import VueRouter from 'vue-router';
import Landing from '@/components/landing/Landing';
import LandingRegistration from '@/components/landing/registration/LandingRegistration';
import { store } from '@/store/store';

Vue.use(VueRouter);

const routes = [
    { path: '*', redirect: '/' },
    {
        name: 'Landing',
        path: '/',
        component: Landing,
        beforeEnter(to, from, next) {
            const totalSteps = store.getters.getRegistrationTotalSteps;
            const lastStep = store.getters.getRegistrationLastStep;
            const currentStep = +to.params.current_step;
            if (!currentStep || currentStep > lastStep) {
                return next(`/register/step/${lastStep}`);
            }
            if (currentStep < 1 || currentStep > totalSteps) {
                return next('/');
            }
            return next();
        },
        children: [
            {
                name: 'LandingRegistration',
                path: 'register/step/:current_step',
                component: LandingRegistration
            }
        ]
    }
];

const router = new VueRouter({
    routes,
    mode: 'history',
    base: '/',
    scrollBehavior(to, from, savedPosition) {
        return savedPosition ? savedPosition : { x: 0, y: 0 };
    }
});

export default router;
