import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { store } from '@/store/store';
import apiMixin from './mixins/api';

Vue.config.productionTip = false;

Vue.mixin(apiMixin);

new Vue({
    router,
    store,
    render: (h) => h(App)
}).$mount('#app');
