import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import requestStatus from './modules/request_status';
import registrationModule from './modules/registration';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        requestStatus,
        registrationModule
    },
    plugins: [
        createPersistedState({
            paths: ['registrationModule']
        })
    ]
});
