const state = {
    registration: {
        totalSteps: 4,
        lastStep: 1,
        userContacts: {
            firstName: '',
            lastName: '',
            phone: ''
        },
        userAddress: {
            street: '',
            houseNumber: '',
            zipCode: '',
            city: ''
        },
        payment: {
            owner: '',
            iban: ''
        },
        paymentDataId: null
    }
};

const getters = {
    getRegistrationTotalSteps: (state) => {
        return state.registration.totalSteps;
    },
    getRegistrationLastStep: (state) => {
        return state.registration.lastStep;
    },
    getRegistrationData: (state) => {
        return state.registration;
    },
    getData: (state) => (dataObject) => {
        return state.registration[dataObject];
    },
    getPaymentDataId: (state) => {
        return state.registration.paymentDataId;
    }
};

const mutations = {
    setRegistrationStep: (state, payload) => {
        if (payload > state.registration.lastStep) {
            state.registration.lastStep = payload;
        }
    },
    setUserContacts: (state, payload) => {
        for (let field in payload) {
            if (state.registration.userContacts[field] !== undefined) {
                state.registration.userContacts[field] = payload[field];
            }
        }
    },
    setUserAddress: (state, payload) => {
        for (let field in payload) {
            if (state.registration.userAddress[field] !== undefined) {
                state.registration.userAddress[field] = payload[field];
            }
        }
    },
    setPayment: (state, payload) => {
        for (let field in payload) {
            if (state.registration.payment[field] !== undefined) {
                state.registration.payment[field] = payload[field];
            }
        }
    },
    setPaymentDataId: (state, payload) => {
        state.registration.paymentDataId = payload;
    },
    clearRegistration: (state) => {
        state.registration = {
            totalSteps: 4,
            lastStep: 1,
            userContacts: {
                firstName: '',
                lastName: '',
                phone: ''
            },
            userAddress: {
                street: '',
                houseNumber: '',
                zipCode: '',
                city: ''
            },
            payment: {
                owner: '',
                iban: ''
            },
            paymentDataId: false
        };
    }
};

const actions = {};

export default {
    state,
    getters,
    mutations,
    actions
};
