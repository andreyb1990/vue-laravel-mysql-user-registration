const state = {
    requestStatus: false
};

const getters = {
    getRequestStatus: (state) => {
        return state.requestStatus;
    }
};

const mutations = {
    setRequestStatus: (state, update) => {
        state.requestStatus = update;
    }
};

export default {
    state,
    getters,
    mutations
};
