import { CancelToken, Instance } from './axios';

export default {
    data() {
        return {
            apiRequests: {}
        };
    },
    created() {},
    methods: {
        // Configs
        getApiConfig(url) {
            let config = {};

            if (CancelToken) {
                this.apiRequests[url] = CancelToken.source();
                config.requestIndex = this.apiRequests.length - 1;
                config.cancelToken = this.apiRequests[url].token;
            }

            return config;
        },
        apiGet(url, queryParams = null, config = { json: false }) {
            if (queryParams != null) {
                url += '?' + new URLSearchParams(queryParams).toString();
            }
            const mergedConfig = { ...this.getApiConfig(url), ...config };
            return Instance.get(url, mergedConfig);
        },
        apiPost(url, data, config = { json: false }) {
            const sendData = config.json ? data : this.generateFormData(data);
            const mergedConfig = { ...this.getApiConfig(url), ...config };
            return Instance.post(url, sendData, mergedConfig);
        },
        apiCancel(requestUrl, message = '') {
            if (this.apiRequests[requestUrl]) {
                this.apiRequests[requestUrl].cancel(message);
                this.apiRequests[requestUrl] = null;
            }
        },
        generateFormData(data) {
            let formData = new FormData();
            for (let fld in data) {
                formData.append(fld, data[fld]);
            }
            return formData;
        }
    }
};
