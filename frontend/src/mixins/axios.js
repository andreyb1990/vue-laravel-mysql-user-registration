import axios from 'axios';
import _ from 'lodash';
import router from '@/router';
import { store } from '@/store/store';

const CancelToken = axios.CancelToken;

const Instance = axios.create({
    baseURL: '/api/v1/'
});

const unauthorizedRedirect = _.debounce(() => {
    store.dispatch('logout');
    router.push('/');
}, 500);

const runningRequests = {
    numberOfRunningRequest: 0,
    add() {
        this.numberOfRunningRequest++;
        store.commit('setRequestStatus', true);
    },
    remove() {
        this.numberOfRunningRequest--;
        if (this.numberOfRunningRequest === 0) {
            store.commit('setRequestStatus', false);
        }
    }
};

Instance.interceptors.request.use(
    (config) => {
        if (!config.withoutLoader) {
            runningRequests.add();
        }
        return config;
    },
    (error) => Promise.reject(error)
);

Instance.interceptors.response.use(
    (response) => {
        if (!response.config.withoutLoader) {
            runningRequests.remove();
        }
        return response;
    },
    (error) => {
        if (!error.config.withoutLoader) {
            runningRequests.remove();
        }
        console.log('error', error);
        if (error.response && error.response.status === 401) {
            unauthorizedRedirect();
        }
        return Promise.reject(error.response);
    }
);

export default Instance;
export { Instance, CancelToken };
