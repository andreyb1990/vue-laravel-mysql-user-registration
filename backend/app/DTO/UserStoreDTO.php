<?php

namespace App\DTO;

use JetBrains\PhpStorm\ArrayShape;

class UserStoreDTO
{
    /** @var string */
    private string $firstName;

    /** @var string */
    private string $lastName;

    /** @var mixed */
    private $phone;

    /** @var mixed */
    private $address;

    /** @var bool */
    private bool $emptyAddress;


    public function __construct(
        string $firstName,
        string $lastName,
        ?string $phone,
        ?array $address
    ) {
        $this->firstName    = $firstName;
        $this->lastName     = $lastName;
        $this->phone        = $phone;
        $this->emptyAddress = true;

        if (isset($address)) {
            foreach ($address as $field => $value) {
                if (!empty($value)) {
                    $this->emptyAddress = false;
                }
                $this->address[$field] = $value;
            }
        }
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    #[ArrayShape(['first_name' => "int|null", 'last_name' => "int", 'phone' => "int"])] public function getContacts(): array
    {
        return [
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'phone' => $this->getPhone()
        ];
    }

    public function isEmptyAddress(): bool
    {
        return $this->emptyAddress;
    }

    public function getAddress(): array
    {
        return $this->address;
    }
}
