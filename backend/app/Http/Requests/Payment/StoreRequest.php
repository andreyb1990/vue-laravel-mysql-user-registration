<?php

namespace App\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'   => 'required|integer',
            'owner'     => 'required|string',
            'iban'      => 'required|string'
        ];
    }
}
