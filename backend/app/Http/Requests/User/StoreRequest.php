<?php

namespace App\Http\Requests\User;

use App\DTO\UserStoreDTO;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name'            => 'required|string',
            'last_name'             => 'required|string',
            'phone'                 => 'sometimes|nullable|string',
            'address.street'        => 'sometimes|nullable|string',
            'address.house_number'  => 'sometimes|nullable|string',
            'address.zip_code'      => 'sometimes|nullable|string',
            'address.city'          => 'sometimes|nullable|string',
        ];
    }

    public function dto(): UserStoreDTO
    {
        return new UserStoreDTO(
            $this->get('first_name'),
            $this->get('last_name'),
            $this->get('phone'),
            $this->get('address')
        );
    }
}
