<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreRequest;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function store(StoreRequest $request): JsonResponse
    {
        $dto = $request->dto();

        $user = User::create($dto->getContacts());

        if (!$dto->isEmptyAddress()) {
            UserAddress::create(array_merge($dto->getAddress(), ['user_id' => $user->id]));
            $user->load('address');
        }

        return response()->json($user);
    }
}
