<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\StoreRequest;
use App\Models\Payment;
use Illuminate\Http\JsonResponse;

class PaymentController extends Controller
{
    public function store(StoreRequest $request): JsonResponse
    {
        $payment = Payment::create([
            'user_id' => $request->get('user_id'),
            'owner' => $request->get('owner'),
            'iban' => $request->get('iban')
        ]);

        $payment->send();

        return response()->json($payment);
    }
}
