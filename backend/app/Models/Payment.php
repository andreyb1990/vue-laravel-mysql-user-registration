<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Http;

class Payment extends Model
{
    protected $fillable = [
        'user_id',
        'owner',
        'iban',
        'status',
        'payment_data_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function send(): void
    {
        $paymentResponse = Http::post(env('PAYMENT_API'), [
            'customerId'    => $this->user_id,
            'iban'          => $this->iban,
            'owner'         => $this->owner
        ]);
        if ($paymentResponse->status() == 200) {
            $json = $paymentResponse->json();
            $this->payment_data_id = $json['paymentDataId'];
            $this->status = 'success';
        } else {
            $this->status = 'fail';
        }
        $this->save();
    }
}
