<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class User extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'phone'
    ];

    public function address(): HasOne
    {
        return $this->hasOne(UserAddress::class);
    }

    public function payment(): HasOne
    {
        return $this->hasOne(Payment::class);
    }
}
